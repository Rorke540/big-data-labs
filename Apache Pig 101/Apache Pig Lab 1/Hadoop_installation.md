![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/skillup.png)

# APACHE HADOOP INSTALLATION:

### HADOOP DOWNLOAD:

**Step 1:** : Follow this link to [Download Hadoop](https://archive.apache.org/dist/hadoop/common/hadoop-3.1.2/hadoop-3.1.2.tar.gz)

As you click on Download Hadoop, hadoop-3.1.2.tar.gz file will be downloaded. 


**Step 2:**  Extract it using the appropriate tool for your OS.

**NOTE:** For Windows, you can use **WinRAR** to extract the file. It is available for download from this [link](https://www.win-rar.com/postdownload.html?&L=0) . For Mac use **The Unarchiver** from the app store. For Linux use `tar -xf hadoop-3.1.2.tar.gz` on the command prompt, from the directory where the file is located or giving the full path of the file.

If you are using WinRAR, click on extract file and a extraction path and options page will open as shown in the image below.

Choose the destination path on your local system. In this example C:\ has been used to extract.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_29.png"  width="600">

**After the extraction is completed, you will get this diagnostic message.Kindly close it.**

**Step 3:** Then you will see Hadoop folder created. Open this folder. All Hadoop files are
inside this folder.

### EDITION OF HADOOP etc FILE:

To edit hadoop file you will need a text editor. For Windows we can use <a href="https://en.softonic.com/download/notepad-plus/windows/post-download" target="_blank">Notepad++</a>.


**Lets start with editing now.**

**Step 1:** Open **Hadoop** folder --> open **etc** folder -->  open **hadoop** folder:

We are going to edit 5 files here.

**core-site**

Right click on this file and select option edit with notepad++ or any text editor preferred.

Copy and paste this value given below inside the configuration tag in core-site.xml file.

    
    <property>

        <name>fs.defaultFS</name>

        <value>hdfs://localhost:9000</value>

    </property>
`

After pasting it, it will look like this.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_32.png"  width="600">

NOTE: Don’t forget to save all the files after you paste your values in it. Save it after editing.

**Hdfs-site.xml**

Before editing this file, go to Hadoop folder in your local system, in the case of this example,  C:\ drive and create a new folder named as data.

Inside data folder create 2 folders **namenode** and **datanode**.  We will paste full path of
your datanode and namenode in value as shown in the example below.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_33.png"  width="600">


**Hdfs-site.xml**
   

    <property>

        <name>dfs.replication</name>

        <value>1</value>

    </property>

    <property>

        <name>dfs.namenode.name.dir</name>

        <value>C:\HADOOP\data\namenode</value>

    </property>

    <property>

        <name>dfs.datanode.data.dir</name>

        <value>C:\HADOOP\data\datanode</value>

    </property>



**Mapred-site.xml**

    <property>

        <name>mapreduce.framework.name</name>

        <value>yarn</value>

    </property>

**Yarn-site.xml**
    
    <property>

        <name>yarn.nodemanager.aux-services</name>

        <value>mapreduce_shuffle</value>

    </property>

    <property>

        <name>yarn.nodemanager.auxservices.mapreduce.shuffle.class</name>

        <value>org.apache.hadoop.mapred.ShuffleHandler</value>

    </property>

**Now open Hadoop-env (window command file) and edit it with notepad++.**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_34.png"  width="600">

Edit here **JAVA_HOME** to be set to the full path of the jdk folder which contains bin. Save
and exit.

### SETTING UP THE ENVIRONMENT VARIABLES FOR HADOOP:

**Step 1:**Click on Start and go to **Settings** -->System and then search for **Edit Environment
Variables** for Your accountand click ok --> Click on **“New”** in User Variables section.

Write variable name as: **“HADOOP_HOME”** and variable value is the path location of bin.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_35.png"  width="600">

**Step 2:** Then click on **“Path”** in System Variables, select **“New”** enter the path of Hadoop's
bin folder. This is done for the system to recognize the hadoop executables through the
command-line.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_36.png"  width="600">

**Step 3:** Then add the path location of hadoop’s sbin folder as well and click ok.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_37.png"  width="600">

**In Hadoop we are missing some configuration file, let’s fix that**

**Step 1:** Click on this link https://drive.google.com/file/d/1AMqV4F5ybPF4ab4CeK8B3AsjdGtQCdvy/view or copy paste this link on new tab. Download the rar file.

**Step 2:** From download folder copy this file and paste it inside the Hadoop folder.

**Step 3:** Extract this Hadoop configuration file using the appropriate tool for your OS. 

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_38.png"  width="600">

**Step 4:** A new folder named as **“HadoopConfiguration-FIXbin”** is created. Open it, copy the bin folder in it and **replace** the hadoop bin folder, with this bin folder. 

### Veryfying the installation of HADOOP(COMMAND PROMPT)

**Step 1:** Open command prompt in **Administrative mode** (Press Windows+R to open the “Run” box. Type “cmd” into the box and then press **Ctrl+Shift+Enter** to run the command as an administrator) and change the directory to C:\Hadoop\sbin and type `hadoop version` and press enter.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_39.png"  width="600">

**Step 2:** Type `hdfs namenode -format` and press enter.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_40.png"  width="600">

**Step 3:** Type `start-all.cmd` and press enter.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_41.png"  width="600">

### Verify Hadoop installation via browser

On the browser enter the address **http://localhost:8088/**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_42.png"  width="600">


On the browser enter the address **http://localhost:9870/**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_43.png"  width="600">

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/had_44.png"  width="600">

**Hadoop has been successfully installed.**

                                         **This ends the exercise**





















