# Hands on Lab Exercise:Installation of MySQL

### Objective:

To download and install MySQL for creating database locally.

### Installation:
- SQL stands for Structured Query Language which is a set of commands used to store and retrieve information using a relational database.

- MySQL is a type of relational database and there are many others such as Microsoft SQL Server, IBM DB2 and Microsoft Access. 

- This lesson uses MySQL since it is open-source, used by a large community, has a long track record and has a freely downloadable version.

Click on this link: [Download MySQL](https://dev.mysql.com/get/Downloads/MySQLInstaller/mysql-installer-web-community-8.0.21.0.msi) for downloading the MySQL.

-Using the **MySQL Installer** for Windows is the recommended way to install the components of MySQL. 

-Once the file is downloaded, double click on the downloaded file to install it. Follow the prompts to accept the licence. After the products are installed, you will be prompted for options:

**Step 1:** Choosing a Setup Type

Select: **Developer Default**.**This installs the MySQL Server and the tools required for MySQL application development. This is useful if you intend to develop applications for an existing server. (See below)

<img src="images/1.png"  width="600">

**Step 2. Check Requirements**

Click the **Execute** button if you have *failing requirements* listed under Check Requirements. Your list of requirements may be different than what is shown here. Once the execute process installs the missing requirements click the Next button. (See below)

<img src="images/2.png"  width="600">

**Step 3. Type and Networking**

Select: **Standalone MySQL Server** (See below)

<img src="images/3.png"  width="600">

**Step 4. Type and Networking**

Config type: Select: **Development Machine** Check: TCP/IP. Port number: 3306. (See below)

<img src="images/4.png"  width="600">

**Step 5. Accounts and Roles**

<img src="images/5.png"  width="600">

**Step 6. Windows Service**

Settings here are optional, but I find it easier to set up MySQL as a Windows Service and have it start automatically. A Windows Service is a process that runs on your computer while you are working. You can change your Windows Service settings later to start the MySQL service manually so that it does not start when you don’t need it.

<img src="images/6.png"  width="600">

Click the Execute and Next buttons to finish the installation and start the server.

**Step 7. Root password**

When prompted for the root password, enter the root password you wrote down in step 5. Accounts and Roles above. (See below)

<img src="images/7.png"  width="600">

Look in the Start menu under MySQL for MySQL Workbench. If it is there, click to launch. If not, click on MySQL Installer - Community to re-run the installation and add MySQL Workbench to the installed products.

**Step 8. Executing some basic Commands.**

Kindly open the MySQL Command Line Client – Unicode.

<img src="images/8.png"  width="600">

Kindly enter the password you set before.

<img src="images/9.png"  width="600">

You can see there are some command already shown like ‘help;’, lets try that:

<img src="images/10.png"  width="600">

This shows all the major command used in MySQL.

Let’s find out the default databases that already in MySQL which we will use in the next lab.

Type the following command:

**show databases;**

<img src="images/11.png"  width="600">

We will use one of these databases in the future labs.

                                                 **This Ends the Exercise**















